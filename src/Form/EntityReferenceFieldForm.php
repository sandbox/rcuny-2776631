<?php

namespace Drupal\ds_entityreference_field\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ds\Form\FieldFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EntityReferenceFieldForm extends FieldFormBase {
  /**
   * The type of the display suite field.
   */
  const TYPE = 'entityreference_field';

  /**
   * Holds the entity field manager
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Holds the entity type repository
   *
   * @var \Drupal\Core\Entity\EntityTypeRepositoryInterface
   */
  protected $entityTypeRepository;


//\Drupal\Core\Entity\EntityManagerInterface::getAllBundleInfo()

  /**
   * Constructs a \Drupal\system\CustomFieldFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   *   The cache invalidator.
   * @param \Drupal\Core\Extension\ModuleHandler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityFieldManager
   *   The entity field manager.
   */
  public function __construct(ConfigFactory $config_factory, EntityTypeManagerInterface $entity_type_manager, CacheTagsInvalidatorInterface $cache_invalidator, ModuleHandler $module_handler, EntityFieldManagerInterface $entity_field_manager, EntityTypeRepositoryInterface $entity_type_repository) {
    parent::__construct($config_factory, $entity_type_manager, $cache_invalidator, $module_handler);
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeRepository = $entity_type_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('cache_tags.invalidator'),
      $container->get('module_handler'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return EntityReferenceFieldForm::TYPE;
  }

  /**
   * {@inheritdoc}
   */
  public function getTypeLabel() {
    return 'Entity-reference field';
  }
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ds_entityreference_field_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $field_key = '') {
    $form = parent::buildForm($form, $form_state, $field_key);

    $entityreference_fields = $this->availableEntityReferenceFields();

    $form['entityreference_field'] = [
        '#type' => 'select',
        '#title' => $this->t('EntityReference field'),
        '#description' => $this->t('Choose a vocabulary to be rendered in this field.'),
        '#default_value' => isset($this->field['properties']['entityreference_field']) ? $this->field['properties']['entityreference_field'] : '',
        '#options' => $entityreference_fields,
        '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getProperties(FormStateInterface $form_state) {
    return [
      'entityreference_field' => $form_state->getValue('entityreference_field'),
    ];
  }

  /**
   *
   */
  protected function availableEntityReferenceFields(){
    $entityReferenceFields = array();

    // fetch all entity types, then iterate on the Content group
    foreach ($this->entityTypeRepository->getEntityTypeLabels(TRUE)['Content'] as $entity_type_label => $entity_type){
      // fetch the available fields for the current entity_type
      $field_storage = $this->entityFieldManager->getFieldStorageDefinitions($entity_type_label);
      foreach ($field_storage as $id => $field) {
        if ($field->getType() == "entity_reference") {
          $entityReferenceFields[$field->getSettings('definition')['target_type'] . '__' . $id] = $field->getSettings('definition')['target_type'] . '|' . $id;
        }
      }
    }

    asort($entityReferenceFields);
    return $entityReferenceFields;
  }

}