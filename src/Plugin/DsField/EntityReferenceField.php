<?php
namespace Drupal\ds_entityreference_field\Plugin\DsField;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\ds\Plugin\DsField\Entity;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DsField(
 *   id = "entityreference_field_field",
 *   deriver = "Drupal\ds_entityreference_field\Plugin\Derivative\EntityReferenceField"
 * )
 */
class EntityReferenceField extends Entity implements ContainerFactoryPluginInterface {
  /**
   * Holds the entity field manager
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Holds the entity type manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Holds the name of the entity reference field that we are cloning
   */
  protected $entityreference_field;

  /**
   * Holds the type of the entities referenced by the field that we are cloning
   */
  protected $entityreference_type;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct($configuration, $plugin_id, $plugin_definition,
                              EntityTypeManagerInterface $entity_type_manager,
                              EntityFieldManagerInterface $entity_field_manager,
                              EntityDisplayRepositoryInterface $entity_display_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_display_repository);
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    if (isset($configuration['field']['properties']['entityreference_field']) && ($separator = strpos($configuration['field']['properties']['entityreference_field'], "__"))){
      $this->entityreference_field = substr($configuration['field']['properties']['entityreference_field'], $separator + 2);
      $this->entityreference_type = substr($configuration['field']['properties']['entityreference_field'], 0, $separator);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build_array = array();
    $referenced_entities = null;
    /** @var $host_entity ContentEntityBase */
    $host_entity = $this->entity();

    // currently supports node references, term references, block references, parent taxonomy term
    //todo: @support view building for more entity-reference types

    // check hasField() before trying to get() the field, otherwise it may break
    if ($this->getEntityViewMode() && $host_entity->hasField($this->entityreference_field)){
      // special case to traverse terms through the parent relation
      if ($this->entityreference_field == "parent"){
        $referenced_entities = $this->entityTypeManager->getStorage('taxonomy_term')->loadParents($host_entity->id());
      } else {
        $reference_list = $host_entity->get($this->entityreference_field);
        $referenced_entities = $reference_list ? $reference_list->referencedEntities() : null;
      }

      foreach ($referenced_entities as $entity) {
        $build_array[] = $this->entityTypeManager->getViewBuilder($this->entityreference_type)->view($entity, $this->getEntityViewMode());
      }
    }

    return $build_array;
  }

  /**
   * Gets the wanted entity
   */
  public function linkedEntity() {
    return $this->entityreference_type;
  }

  /**
   * {@inheritdoc}
   */
  public function formatters() {
    $formatters = array(
      'entity_reference_entity_view' => t('Rendered entity')
    );

    return $formatters;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm($form, FormStateInterface $form_state) {
    $parent_form = parent::settingsForm($form, $form_state);

    $form['help_field'] = array(
      '#markup' => t('Field') . ': ' . $this->entityreference_field . '<br>',
    );
    $form['help_entity_type'] = array(
      '#markup' => t('Entity type') . ': ' . $this->entityreference_type,
    );

    return array_merge($form, $parent_form);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings) {
    $parent_summary = array();
    $config = $this->getConfiguration();

    if (!empty($config['entity_view_mode'])) {
      $parent_summary = parent::settingsSummary($settings);
    } else {
      $parent_summary[] = 'View mode: undefined';
    }

    $summary = array();
    $summary[] = 'Field: ' . $this->entityreference_field;
    $summary[] = 'Entity  type: ' . $this->entityreference_type;

    return array_merge($summary, $parent_summary);
  }

  /**
   * {@inheritdoc}
   */
  public function isAllowed() {
    $entity_has_field = null;
    $host_entity_type = $this->getEntityTypeId();
    $host_entity_bundle = $this->bundle();

    if ($host_entity_type && $host_entity_bundle) {
      // fetch the available fields definition for the current entity_type / bundle
      $field_storage = $this->entityFieldManager->getFieldDefinitions($host_entity_type, $host_entity_bundle);
      //  check that this entity has the field that we plan to display
      $entity_has_field = isset($field_storage[$this->entityreference_field]);
    }

    // combine check: entity_has_field + conditions from parent isAllowed method;
    // this ensure we can still override on a per field basis through configuration
    return $entity_has_field && parent::isAllowed();
  }

}