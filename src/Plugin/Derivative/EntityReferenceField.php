<?php
namespace Drupal\ds_entityreference_field\Plugin\Derivative;

use Drupal\ds\Plugin\Derivative\DynamicField;
use Drupal\ds_entityreference_field\Form\EntityReferenceFieldForm;

class EntityReferenceField extends DynamicField {
  /**
   * {@inheritdoc}
   */
  protected function getType() {
    return EntityReferenceFieldForm::TYPE;
  }
}